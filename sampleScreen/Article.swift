//
//  Article.swift
//  5+1
//
//  Created by Tatiana Tishchenko on 17/02/17.
//  Copyright © 2017 Tatiana Tishchenko. All rights reserved.
//

import UIKit

class Article {

    var ImageSt: String
    var FullImageSt: String
    var Image: UIImage?
    var FullImage: UIImage?
    var Title: String
    var FullTitle: String
    var Url: String
    
    init (Title: String, FullTitle: String, ImageSt: String, FullImageSt: String, Image: UIImage?, FullImage: UIImage?, Url:String) {
//    init (Title: String, FullTitle: String){
        self.Title = Title
        self.FullTitle = FullTitle
        self.ImageSt = ImageSt
        self.FullImageSt = FullImageSt
        self.Image = Image
        self.FullImage = FullImage
        self.Url = Url
    }

}
// $(PRODUCT_BUNDLE_IDENTIFIER)
