//
//  RequestDataViewController.swift
//  5+1
//
//  Created by Tatiana on 24.02.17.
//  Copyright © 2017 Tatiana Tishchenko. All rights reserved.
//

import UIKit

class RequestDataViewController: UIViewController {

    var appContentID  = 0

    @IBOutlet weak var infoLabel: UILabel!
    @IBAction func checkForUpdates(_ sender: Any) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        
        infoLabel.text = self.infoLabel.text! + "\n\nЗапрашиваю contentID на серевере (www.racoonapps.ru/id.json)..."
        
        //формируем запрос к серверу. Проверяем ID
        //1.Формируем  URL
        if let url = URL(string: "http://fiveplusone.zeppelin-creative.ru/story.getday") {
            
            //2.Из URL формируем запрос
            let request = NSMutableURLRequest(url: url)
            //3. Из запроса создаем задачу(task)
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                //задача возвращает 3 переменные: данные, ответ и возможно ошибку
                if error != nil {
                    
                    print(error!)
                    
                } else {
                    //3.Если нет ошибки - конвертируем в json
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: [])
                    
                    
                        if let dictionary = json as? [String: Any] {
                            if let numberTS = dictionary["timestamp"] as? Int {
                                //4.сравниваем полученный ID с сохраненным в приложении
                                
                                print(numberTS)
                                
                                // запрос статьи на сервере
                                let serverDay = formatter.string(from: Date(timeIntervalSince1970: TimeInterval(numberTS)))
                                let appDay = formatter.string(from: Date())
                                
                                print("ServerDay: " + serverDay)
                                print("AppDay:", appDay)
                                
                                //if (serverDay != appDay) {
                                if (true) {
                                    
                                    //запросы к серверу делаются в отдельном потоке, поэтому когда мы доходим до изменения лейблов, нам нужно явно указать что это будет делаться рамках главного потока (main thread)
                                    DispatchQueue.main.sync(execute: {
                                        
                                        //если на сервере ID больше, то надо обновлять
                                        self.infoLabel.text = self.infoLabel.text! +  "\n\ncontentID на серевере = \(numberTS) - необходимо обновить контент\n\nЗапрашиваю статью (Article) на серевере (www.racoonapps.ru/article.json)..."
                                        
                                    })
                                    
                                    //5.Получение Article на сервере
                                    let url2 = URL(string: "http://www.racoonapps.ru/article.json")!
                                    
                                    let task2 = URLSession.shared.dataTask(with: url2) {
                                        data, response, error in
                                        //задача возвращает 3 переменные: данные, ответ и возможно ошибку
                                        if error != nil {
                                            print(error!)
                                            
                                        } else {
                                            //6.Если нет ошибок выводим статьи на экран
                                            if let unwrappedData = data {
                                                do {
                                                    //получение в виде JSON для работы с отдельными полями
                                                    let dataJSON = try JSONSerialization.jsonObject(with: unwrappedData, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, Any>
                                                    
                                                    //как получить отдельные поля??? это жесть!!!!
                                                    if let title0 = ((dataJSON?["articles"] as? NSArray)?[0] as? NSDictionary)?["title"] as? String {
                                                        print(title0)
                                                    }
                                                    
                                                    
                                                    //вывод в лейбл в виде строки
                                                    let dataString = String(data: unwrappedData, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                                                    DispatchQueue.main.sync(execute: {
                                                        //не нужно обновлять
                                                        self.infoLabel.text = self.infoLabel.text! +  "\n\n" + dataString!
                                                    })
                                                    
                                                    self.appContentID = numberTS
                                                } catch {
                                                    print ("some error")
                                                }
                                            }
                                        }
                                    }
                                    
                                    task2.resume()
                                    
                                }
                                else {
                                
                                    DispatchQueue.main.sync(execute: {
                                        //не нужно обновлять
                                            self.infoLabel.text = self.infoLabel.text! +  "\n\n" + "contentID на серевере = \(numberTS) - обновление не нужно"
                                    })
                                
                                }
                                
                            }
                            
    //                        for (key, value) in dictionary {
    //                            // access all key / value pairs in dictionary
    //                        }
    //                        
    //                        if let nestedDictionary = dictionary["anotherKey"] as? [String: Any] {
    //                            // access nested dictionary values by key
    //                        }
                        }
                        
    //                    if let unwrappedData = data {
    //                        print(response)
    //                        print(unwrappedData)
    //                        let dataString = NSString(data: unwrappedData, encoding: String.Encoding.utf8.rawValue)
    //                    
    //                        print(dataString)
    //                    
    //                    }
                    } catch {
                        
                        print ("can't get data")
                        
                    }
                    
                }
            }
            
            task.resume()
            
        }
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        infoLabel.text = "Текущий contentID приложения = \(appContentID)."
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
 
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
