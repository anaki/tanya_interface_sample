//
//  MainViewController.swift
//  5+1
//
//  Created by Tatiana Tishchenko on 17/02/17.
//  Copyright © 2017 Tatiana Tishchenko. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    
    @IBOutlet weak var headerDay: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    
    var articleBlockHeight: CGFloat = 95.0
    let titleGap: CGFloat = 20.0
    let collapseDirection = [1,1,1,1,-1,-1]
    var activeView : Int = 0
    public var viewNum :Int = 0
    let formatter = DateFormatter()
    public var articles: [Article] = []
    
    let fileManager = FileManager.default
    
    override func viewDidLayoutSubviews() {
        //при запуске viewDidLayoutSubviews уже отработали constraints и view занимает столько сколько должна на экране - можно узнать размер
        //корректируем размер view
        stackView.setNeedsLayout()
        stackView.layoutIfNeeded()
        
        articleBlockHeight = findViewSize(tag: 1).height
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print ("viewDidLoad")

        loadData()
        
        //день недели в заголовок
        let headerdateFormatter = DateFormatter()
        headerdateFormatter.dateFormat = "EEEE"
        headerDay.text = headerdateFormatter.string(from: Date()).capitalized
//        if headerDay.text == "Monday" {
//            headerDay.text = "Понедельник"
//        } else if headerDay.text == "Tuesday" {
//            headerDay.text = "Вторник"
//        } else if headerDay.text == "Wednesday" {
//            headerDay.text = "Среда"
//        }else if headerDay.text == "Thursday" {
//            headerDay.text = "Четверг"
//        } else if headerDay.text == "Friday" {
//            headerDay.text = "Пятница"
//        } else if headerDay.text == "Saturday" {
//            headerDay.text = "Суббота"
//        } else {
//            headerDay.text = "Воскресение"
//        }

        

    }
    
    
    func loadData() {
        
        formatter.dateFormat = "dd.MM.yyyy"
        
        //получаем appTimestamp
        var appTimestamp = 0
        if let gettingTS = UserDefaults.standard.object(forKey: "appTimestamp") as? Int {
            appTimestamp = gettingTS
        }
        else {
            appTimestamp = Int(Date().timeIntervalSince1970)
        }
        
        //если есть сохраненные статьи - загружаем
        var appArticles = [Dictionary<String, String>]()
        if let gettingArticles = UserDefaults.standard.array(forKey: "articles") {
            appArticles = gettingArticles as! [Dictionary]
        }
        
        print("appTimestamp", appTimestamp)
        print("appArticles", appArticles.count)
        
        // сравниваем appTimestamp с серверным
        if let url = URL(string: "http://fiveplusone.zeppelin-creative.ru/story.getday") {
            
            //2.Из URL формируем запрос
            let request = NSMutableURLRequest(url: url)
            //3. Из запроса создаем задачу(task)
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                //задача возвращает 3 переменные: данные, ответ и возможно ошибку
                if error != nil {
                    
                    print(error!)
                    
                } else {
                    //3.Если нет ошибки - конвертируем в json
                    do {
                        let json = try JSONSerialization.jsonObject(with: data!, options: [])
                        
                        if let dictionary = json as? [String: Any] {
                            if let serverTimstamp = dictionary["timestamp"] as? Int {
                                
                                print("serverTimstamp", serverTimstamp)
                                
                                //4.сравниваем полученный Timstamp с сохраненным в приложении
                                if (appTimestamp != serverTimstamp || appArticles.count == 0) {
                                //if (true) {
                                    
                                    //5.Получение Article на сервере http://www.racoonapps.ru/article.json
                                    let url2 = URL(string: "http://fiveplusone.zeppelin-creative.ru/story.getdatabyday")!
                                    
                                    let task2 = URLSession.shared.dataTask(with: url2) {
                                        data, response, error in
                                        //задача возвращает 3 переменные: данные, ответ и возможно ошибку
                                        if error != nil {
                                            print(error!)
                                            
                                        } else {
                                            //6.Если нет ошибок выводим статьи на экран
                                            if let unwrappedData = data {
                                                do {
                                                    //получение в виде JSON для работы с отдельными полями
                                                    let dataJSON = try JSONSerialization.jsonObject(with: unwrappedData, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, Any>

                                                    if let items = dataJSON?["data"] as? NSArray {
                                                        
                                                        print("All items")
                                                        
                                                        //переменная чтобы закешировать запрос
                                                        var dictionaryToSave = [Dictionary<String, String>]()
                                                        
                                                        for item in items {
                                                            print(item)
                                                            var title = "", fulltitle = ""
                                                            var littelimg = "", fullimage = "", urlstory =  ""
                                                            
                                                            if let gettingtitle = (item as? NSDictionary)?["title_short"] as? String {
                                                                title = gettingtitle
                                                            }
                                                            if let gettingfulltitle = (item as? NSDictionary)?["title_long"] as? String {
                                                                fulltitle = gettingfulltitle
                                                            }
                                                            if let gettingimg = (item as? NSDictionary)?["pic_small"] as? String {
                                                                littelimg = gettingimg
                                                            }
                                                            if let gettingfullimg = (item as? NSDictionary)?["pic_big"] as? String {
                                                                fullimage = gettingfullimg
                                                            }
                                                            if let gettingurl = (item as? NSDictionary)?["story"] as? String {
                                                                urlstory =  gettingurl
                                                            }
                                                            
                                                            self.articles.append(Article(Title: title, FullTitle: fulltitle, ImageSt: littelimg, FullImageSt: fullimage, Image: nil, FullImage: nil, Url:urlstory))
                                                            dictionaryToSave.append(["title_short" : title, "title_long": fulltitle, "pic_small" : littelimg, "pic_big" : fullimage, "story" : urlstory])
                                                            //let dataString = String(data: unwrappedData, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                                                        }
                                                        
                                                        //запросы к серверу делаются в отдельном потоке, поэтому когда мы доходим до изменения лейблов, нам нужно явно указать что это будет делаться рамках главного потока (main thread)
                                                        DispatchQueue.main.sync(execute: {
                                                            
                                                            self.setArticles()
                                                            
                                                        })
                                                        
                                                        UserDefaults.standard.set(dictionaryToSave, forKey: "articles")
                                                        UserDefaults.standard.set(serverTimstamp, forKey: "appTimestamp")
                                                    }
  

                                                } catch {
                                                    print ("some error")
                                                }
                                            }
                                        }
                                    }
                                    
                                    task2.resume()
                                    
                                }
                                else {
                                    print("обновление не нужно, используем appArticles");
                                    
                                    for item in appArticles {
                                        print("title", item["title_short"])
                                        self.articles.append(Article(Title: item["title_short"]!, FullTitle: item["title_long"]!, ImageSt: item["pic_small"]!, FullImageSt: item["pic_big"]!, Image: nil, FullImage: nil, Url:item["story"]!))
                                    }
                                    
                                    //запросы к серверу делаются в отдельном потоке, поэтому когда мы доходим до изменения лейблов, нам нужно явно указать что это будет делаться рамках главного потока (main thread)
                                    DispatchQueue.main.sync(execute: {
                                        self.setArticles()
                                    })
                                }
                            }
                            
                        }
                        
                    } catch {
                        
                        print ("can't get data")
                        
                    }
                    
                }
            }
            
            task.resume()
            
        }
        
    }
    
    func setArticles () {
        //в методе устанавливаются значения лейблов, навешиваются GestureRecognizerы
        
        //получаем все view вложенные в stackView
        let subviews: NSArray = self.stackView.subviews as NSArray
        if (subviews.count > 0) {
            
            //приципляем ко всем view типа UIView распознователи жеста Tap
            for subview in subviews {
                
                if let articleView = subview as? UIView {
                    
                    if articleView.tag != 0 {
                    
                        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapBlock(_:)))
                        articleView.addGestureRecognizer(tapGesture)
                        
                        let swipeRecognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeLeft(_:)))
                        swipeRecognizer.direction = UISwipeGestureRecognizerDirection.left
                        articleView.addGestureRecognizer(swipeRecognizer)
                        
                        let swipeRecognizerRight: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRight(_:)))
                        swipeRecognizerRight.direction = UISwipeGestureRecognizerDirection.right
                        articleView.addGestureRecognizer(swipeRecognizerRight)
                        
    //                    loadImg(ImgUrlString: articles[articleView.tag - 1].ImageSt, FullimgUrlString: articles[articleView.tag - 1].FullImageSt, NumOfView: articleView.tag)
                        
                        
                        let viewLabel = self.findSubLabel(view: articleView, range: 1)
                        viewLabel.text? = self.articles[articleView.tag - 1].Title
                        let viewLabelFull = self.findSubLabel(view: articleView, range: 2)
                        viewLabelFull.text? = self.articles[articleView.tag - 1].FullTitle
                        
                        if (self.articles[articleView.tag - 1].ImageSt != "") {
                            processImage(forView: self.findImg(view: articleView, range: 2), imageName: self.articles[articleView.tag - 1].ImageSt)
                        }
                        
                        if (self.articles[articleView.tag - 1].FullImageSt != "") {
                            processImage(forView: self.findImg(view: articleView, range: 1), imageName: self.articles[articleView.tag - 1].FullImageSt)
                        }
                    }
                }
            }
            
        }
    
    
    }
    
    func processImage(forView: UIImageView, imageName: String) {
        //смотрим есть ли закешированная картинка или надо качать, после чего устанавливаем значение UIImageView
        
        //имя для запоминания на девайсе
        var local_name = imageName.replacingOccurrences(of: "http://fiveplusone.zeppelin-creative.ru/", with: "", options: .literal, range: nil)
        local_name = local_name.replacingOccurrences(of: "/", with: "", options: .literal, range: nil)
        
        print("imageName", imageName)
        print("local_name", local_name)
        
        //путь до пользовательской директории на девайсе, куда можно сохранить
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        
        
        if documentsPath.count > 0 {
            //директория
            let documentsDirectory = documentsPath[0]
            //полный путь до картинки
            let documentsPathFull = documentsDirectory + "/" + local_name
            
            //проверяем есть ли картинка уже такая
            if fileManager.fileExists(atPath: documentsPathFull) {
                
                //уже есть такая - подставляем и выходим из ф-ции
                print("FILE AVAILABLE")
                forView.image = UIImage(contentsOfFile: documentsPathFull)
             
                return
            }
        }
        
                
        // нет картинки - загружаем по URL
        print("FILE NOT AVAILABLE")
        
        let request = NSMutableURLRequest(url: URL(string: imageName)!)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print(error!)
            } else {
                
                if let data = data {
                    
                    if let newImage = UIImage(data: data) {
                        //подставляем картинку
                        DispatchQueue.main.sync(execute: {
                            forView.image = newImage
                        })
                        
                        //сохраняем ее на девайс
                        if documentsPath.count > 0 {

                            let savePath = documentsPath[0] + "/" + local_name
                            do {
                                //сама запись
                                try UIImageJPEGRepresentation(newImage, 1)?.write(to: URL(fileURLWithPath: savePath))
                                
                            } catch {
                                // process error
                            }
                        }
                    }
                }
            }
        }
        task.resume()
        
    }
    
//    func loadImg(ImgUrlString: String, FullimgUrlString: String, NumOfView: Int){
//        let imageUrl = NSURL(string: ImgUrlString)!
//        let fullimageUrl = NSURL(string: FullimgUrlString)!
////        let imageFromUrlRequest: NSURLRequest = NSURLRequest(url: imageUrl)
//       
//        let data = NSData(contentsOf:imageUrl as URL)
//        let fulldata = NSData(contentsOf: fullimageUrl as URL)
//        articles[NumOfView - 1].Image = UIImage(data: data! as Data)
//        articles[NumOfView - 1].FullImage = UIImage(data: fulldata! as Data)
//        
//    }
    
    
    func tapBlock(_ sender: UITapGestureRecognizer) {
        //вызывается после нажатия на UIView
        print("activeView before " + String(activeView))
        
        if activeView != 0 {
            resetViews()
        }
        
        
        if (activeView != sender.view!.tag) {
            
            let pullingView = findPairView(viewId: sender.view!.tag)
            activeView = sender.view!.tag
            
            if (collapseDirection[sender.view!.tag-1] == -1) {
                //view двигается вверх
                viewGoesUp(pushingView: sender.view!, pullingView: pullingView, hideLabel: true)
            }
            else {
                //view двигается вниз
                viewGoesDown(pushingView: sender.view!, pullingView: pullingView, hideLabel: true)
                
            }
            
        }
        else {
            activeView = 0
        }
        
        print("activeView " + String(activeView))
        
        

    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print ("viewDidAppear")

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Details" {
            if let nextViewController = segue.destination as? ArticleViewController {
                nextViewController.articleNum = activeView
                nextViewController.viewNum = 1
                nextViewController.articles = articles
                
            }
        }
        
    }

    
    @objc func swipeLeft (_ sender: UISwipeGestureRecognizer) {
        print(sender)
        if let tmpView = sender.view as UIView? {
            print(tmpView.tag)
            activeView = tmpView.tag
        }
        //self.performSegue(withIdentifier: "Details", sender: self)

        self.performSegue(withIdentifier: "Details", sender: self)
    }
    
    func swipeRight (_ sender: UISwipeGestureRecognizer){
        self.performSegue(withIdentifier: "HomeToStories", sender: self)
    }
    
    func findViewByTag(tag: Int) -> UIView {
        //найти UIView по значению поля tag
        //если не найдено, то будет возвращен пустой UIView, создаем его
        var returnView : UIView = UIView.init()
        
        //получаем все view вложенные в stackView
        let subviews: NSArray = stackView.subviews as NSArray
        //если нет таких возвращаем пустой UIImageView
        if (subviews.count == 0) {return returnView}
        
        //ищем UIView с нужным значением поля tag
        for subview in subviews {
            if let articleView = subview as? UIView {
                if articleView.tag == tag {
                    returnView = articleView
                    break
                }
            }
        }
        return returnView
    }
    
    func findViewSize(tag: Int) -> CGSize {
        //найти размер UIView со значением поля tag
        //если не найдено, то будет возвращен 0,0
        var returnSize : CGSize = CGSize(width: 0, height: 0)
        
        //получаем все view вложенные в stackView
        let subviews: NSArray = stackView.subviews as NSArray
        //если нет таких возвращаем пустой UIImageView
        if (subviews.count == 0) {return returnSize}
        
        //ищем UIView с нужным значением поля tag
        for subview in subviews {
            if let articleView = subview as? UIView {
                if articleView.tag == tag {
                    returnSize = articleView.bounds.size
                    break
                }
            }
        }
        return returnSize
    }
    
    //ищем парный view
    func findPairView(viewId : Int) -> UIView {
        
        var pairId = 0
        
        if (collapseDirection[viewId-1] == -1) {
            pairId = viewId - 1
        }
        else {
            pairId = viewId + 1
        }
        print(pairId)
        return findViewByTag(tag: pairId)
    }
    
    func findSubLabel(view: UIView, range: Int) -> UILabel {
        //найти UILabel принаждлежащий UIView
        //если не найдено, то будет возвращен пустой UILabel, создаем его
        //range начинается с 1
        let returnView : UILabel = UILabel.init()
        
        var count = 0
        
        //получаем все view вложенные в UIView
        let subviews: NSArray = view.subviews as NSArray
        //если нет таких возвращаем пустой UILabel
        if (subviews.count == 0) {return returnView}
        
        //ищем UILabel
        for subview in subviews {
            if let articleView = subview as? UILabel {
                count += 1
                if (count == range) {
                    return articleView
                }
            }
        }
        return returnView
    }
    
    func findImg(view: UIView, range: Int) -> UIImageView {
        //range начинается с 1
        
        let returnImg : UIImageView = UIImageView.init()
        
        var count = 0
        
        //получаем все view вложенные в UIView
        let subviews: NSArray = view.subviews as NSArray

        for subview in subviews {
            if let articleView = subview as? UIImageView {
                count += 1
                if (count == range) {
                    return articleView
                }
            }
        }
        return returnImg
        
    }
    
    func resetViews () -> Void {
        
        //1. ищем активную UIImageView и его пару
        let pushingView = findViewByTag(tag: activeView)
        let pullingView = findPairView(viewId: activeView)
        
        let pushLabel = findSubLabel(view: pushingView, range: 1)
        let pullingLabel2 = findSubLabel(view: pullingView, range: 1)
        
        UIView.animate(withDuration: 2) {
            pushLabel.isHidden = false
            pullingLabel2.isHidden = false
        }
        
        if (collapseDirection[pushingView.tag-1] != -1) {
            //view двигается вверх
            viewGoesUp(pushingView: pullingView, pullingView: pushingView, hideLabel: false)
            
            
        }
        else {
            //view двигается вниз
            viewGoesDown(pushingView: pullingView, pullingView: pushingView, hideLabel: false)
            
        }
        
    }

    func viewGoesDown (pushingView: UIView, pullingView: UIView, hideLabel: Bool) {
        //view двигается вниз
        print("viewGoesDown")
        print(pushingView)
        
        let pushLabel = findSubLabel(view: pushingView, range: 1)
        
        UIView.animate(withDuration: 0.2) {
            //двигаем вниз верхную границу, размер уменьшаем
            pullingView.frame = CGRect(
                x: pullingView.frame.origin.x,
                y: pullingView.frame.origin.y + self.articleBlockHeight,
                width: pullingView.frame.width,
                height: pullingView.frame.height - self.articleBlockHeight)
            //self.stackView.bringSubview(toFront: pushingView)
            //увеличиваем размер
            pushingView.frame = CGRect(
                x: pushingView.frame.origin.x,
                y: pushingView.frame.origin.y,
                width: pushingView.frame.width,
                height: pushingView.frame.height + self.articleBlockHeight)

            if (hideLabel) {
                    pushLabel.isHidden = true
            }
            self.toggleBackImages(view: pushingView)
            self.toggleBackImages(view: pullingView)
            //двигаем label
           // pushLabel.frame = CGRect(x: pushLabel.frame.origin.x, y: 190.0, width: pushLabel.frame.width, height: pushLabel.frame.height)
        }

    }
    
    func viewGoesUp (pushingView: UIView, pullingView: UIView, hideLabel: Bool) {
        //view двигается вверх
        print("viewGoesUp")
        let pushLabel = findSubLabel(view: pushingView, range: 1)
     //   let pushLabel2 = findSubLabel(view: pushingView, range: 2)
        UIView.animate(withDuration: 0.2) {
            //двигаем вверх верхную границу, размер увеличиваем
            pushingView.frame = CGRect(
                x: pushingView.frame.origin.x,
                y: pushingView.frame.origin.y - self.articleBlockHeight,
                width: pushingView.frame.width,
                height: pushingView.frame.height + self.articleBlockHeight)
            //сжимаемое view - уменьшаем размер
            pullingView.frame = CGRect(
                x: pullingView.frame.origin.x,
                y: pullingView.frame.origin.y,
                width: pullingView.frame.width,
                height: pullingView.frame.height - self.articleBlockHeight)
            
            if (hideLabel) {pushLabel.isHidden = true}
            self.toggleBackImages(view: pushingView)
            self.toggleBackImages(view: pullingView)
        }
    }
    
    func toggleBackImages(view: UIView) {
        //переключаем аттрибут isHidden у картинок фона чтобы подменять их друг ругом 
        
        //получаем все view вложенные в UIView
        let subviews: NSArray = view.subviews as NSArray
        
        for subview in subviews {
            if let articleView = subview as? UIImageView {

                if articleView.isHidden {
                    articleView.isHidden = false
                }
                else {
                    articleView.isHidden = true
                }
            }
        }
    
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
