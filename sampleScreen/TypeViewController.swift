//
//  TypeViewController.swift
//  5+1
//
//  Created by Tatiana on 05.03.17.
//  Copyright © 2017 Tatiana Tishchenko. All rights reserved.
//

import UIKit

class TypeViewController: UIViewController {

    @IBOutlet var allView: UIView!
    @IBOutlet weak var popular: UIButton!
    @IBOutlet weak var types: UIStackView!
    public var typeLab: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        popular.layer.cornerRadius = 5
        types.layer.cornerRadius = 30

        // Do any additional setup after loading the view.
        
        let swipeRecognizerRight: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeLeft(_:)))
        swipeRecognizerRight.direction = UISwipeGestureRecognizerDirection.left
        allView.addGestureRecognizer(swipeRecognizerRight)
    }
    
    func swipeLeft (_ sender: UISwipeGestureRecognizer){
        self.performSegue(withIdentifier: "storiesToHome", sender: self)
    }
    
    // передача заголовка в
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "philosophy" {
            if let nextViewController = segue.destination as? StoriesViewController {
                nextViewController.typeLab = "Философия"
            }
        } else if segue.identifier == "business" {
            if let nextViewController = segue.destination as? StoriesViewController {
                nextViewController.typeLab = "Бизнес"
            }
            
        } else if segue.identifier == "art" {
            if let nextViewController = segue.destination as? StoriesViewController {
                nextViewController.typeLab = "Искусство"
            }
            
        } else if segue.identifier == "history" {
            if let nextViewController = segue.destination as? StoriesViewController {
                nextViewController.typeLab = "История"
            }
            
        } else if segue.identifier == "it" {
            if let nextViewController = segue.destination as? StoriesViewController {
                nextViewController.typeLab = "Технологии"
            }
            
        } else if segue.identifier == "popular" {
            if let nextViewController = segue.destination as? StoriesViewController {
                nextViewController.typeLab = "Популярное"
            }
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
