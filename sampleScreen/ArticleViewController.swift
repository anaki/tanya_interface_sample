//
//  ArticleViewController.swift
//  5+1
//
//  Created by Tatiana on 19.02.17.
//  Copyright © 2017 Tatiana Tishchenko. All rights reserved.
//

import UIKit

class ArticleViewController: UIViewController {

    @IBOutlet weak var WebView: UIWebView!
    
    public var articleNum : Int = 0;
    public var viewNum : Int = 0;
    public var typeLab: String = ""
    public var articles: [Article] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        print("Article num")
        print(articleNum)
        // Do any additional setup after loading the view.
        
        let swipeRecognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeDown(_:)))
        swipeRecognizer.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeRecognizer)
        self.WebView.scrollView.panGestureRecognizer.require(toFail: swipeRecognizer)

    }
    
    func swipeDown(_ sender: UISwipeGestureRecognizer){
        // viewNum: если 1, то MainView, если 2, то StoriesView
        if(viewNum == 1){
            self.performSegue(withIdentifier: "goHome", sender: self)
        } else{
            self.performSegue(withIdentifier: "StoryToStories", sender: self)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        WebView.allowsInlineMediaPlayback = true
//        let imgName = "\(articleNum).png"
//        print(imgName)
//        let image = UIImage(named: imgName) as UIImage?
//        imgButton.setBackgroundImage(image, for: [])
        
        if (articleNum == 1){
//            WebView.loadRequest(URLRequest(url: URL(string: "http://zeppelin-creative.ru/news/")!))
            WebView.loadRequest(URLRequest(url: URL(string: articles[articleNum - 1].Url)!))
        } else if(articleNum == 2){
            WebView.loadRequest(URLRequest(url: URL(string: articles[articleNum - 1].Url)!))
        } else if(articleNum == 3){
            WebView.loadRequest(URLRequest(url: URL(string: articles[articleNum - 1].Url)!))
        } else if(articleNum == 4){
            WebView.loadRequest(URLRequest(url: URL(string: articles[articleNum - 1].Url)!))
        } else if(articleNum == 5){
            WebView.loadRequest(URLRequest(url: URL(string: articles[articleNum - 1].Url)!))
        } else {
            WebView.loadRequest(URLRequest(url: URL(string: articles[articleNum - 1].Url)!))
        }

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "StoryToStories" {
            if let nextViewController = segue.destination as? StoriesViewController {
                nextViewController.typeLab = typeLab
            }
        }
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
