//
//  StoriesViewController.swift
//  5+1
//
//  Created by Tatiana on 05.03.17.
//  Copyright © 2017 Tatiana Tishchenko. All rights reserved.
//

import UIKit

class StoriesViewController: UIViewController {
    
    @IBOutlet weak var mainLable: UILabel!
    @IBOutlet weak var storiesView: UIStackView!
    @IBOutlet weak var goh: UIButton!
    
    var activeView : Int = 0
    var articleBlockHeight: CGFloat = 95.0
    let collapseDirection = [1,1,1,1,-1,-1]
    public var viewNum : Int = 0;
    public var typeLab: String = ""
    
    override func viewDidLayoutSubviews() {
        //при запуске viewDidLayoutSubviews уже отработали constraints и view занимает столько сколько должна на экране - можно узнать размер
        //корректируем размер view
        storiesView.setNeedsLayout()
        storiesView.layoutIfNeeded()
        
        articleBlockHeight = findViewSize(tag: 1).height
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        goh.layer.cornerRadius = 15
        mainLable.text = typeLab
        
        // Do any additional setup after loading the view.
        
//        var articles: [Article] = []
//        
//        // ?получаем новые по GET реквесту?
//        articles.append(Article(Title: "Трамп", FullTitle: "Трампа не хотят в Лондоне"))
//        articles.append(Article(Title: "Девочки", FullTitle: "В Лондене прошла вечеринка Playboy"))
//        articles.append(Article(Title: "Футбол", FullTitle: "Тур Лиги Чемпионов"))
//        articles.append(Article(Title: "Первая леди", FullTitle: "Мелания Трамп: 15 фактов о первой леди США"))
//        articles.append(Article(Title: "Где деньги", FullTitle: "Бюджет получил деньги за 19,5% Роснефти"))
//        articles.append(Article(Title: "Адам и Хайп", FullTitle: "Адам и Хайп. Кто это?"))
        
        //записываем в хранилище
        //UserDefaults.standard.set(1, forKey: "ID") //по этому ID будет определяться нужно ли загружать новые данные или у пользователя уже актуальная информация
        //UserDefaults.standard.set(articles, forKey: "articles")
        
        
//        let swipeRecognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeLeft(_:)))
//        swipeRecognizer.direction = UISwipeGestureRecognizerDirection.left
//        storiesView.addGestureRecognizer(swipeRecognizer)
//        
//        let swipeRecognizerRight: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRight(_:)))
//        swipeRecognizerRight.direction = UISwipeGestureRecognizerDirection.right
//        storiesView.addGestureRecognizer(swipeRecognizerRight)
        
        let subviews: NSArray = storiesView.subviews as NSArray
        if (subviews.count > 0) {
            
            //приципляем ко всем view типа UIView распознователи жеста Tap
            for subview in subviews {
                if let articleView = subview as? UIView {
                    
                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapBlock(_:)))
                    articleView.addGestureRecognizer(tapGesture)
                    
                    let swipeRecognizer: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeLeft(_:)))
                    swipeRecognizer.direction = UISwipeGestureRecognizerDirection.left
                    articleView.addGestureRecognizer(swipeRecognizer)
                    
                    let swipeRecognizerRight: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRight(_:)))
                    swipeRecognizerRight.direction = UISwipeGestureRecognizerDirection.right
                    articleView.addGestureRecognizer(swipeRecognizerRight)
                    
//                    let viewLabel = findSubLabel(view: articleView, range: 1)
//                    viewLabel.text? = articles[articleView.tag - 1].Title
//                    let viewLabelFull = findSubLabel(view: articleView, range: 2)
//                    viewLabelFull.text? = articles[articleView.tag - 1].FullTitle
                }
            }
            
        }

     
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "storiesToStory" {
            if let nextViewController = segue.destination as? ArticleViewController {
                nextViewController.articleNum = activeView
                nextViewController.viewNum = 2
                nextViewController.typeLab = mainLable.text!
            }
        }
        
    }
    
    func tapBlock(_ sender: UITapGestureRecognizer) {
        //вызывается после нажатия на UIView
        print("activeView before " + String(activeView))
        
        if activeView != 0 {
            resetViews()
        }
        
        if (activeView != sender.view!.tag) {
            
            let pullingView = findPairView(viewId: sender.view!.tag)
            activeView = sender.view!.tag
            
            if (collapseDirection[sender.view!.tag-1] == -1) {
                //view двигается вверх
                viewGoesUp(pushingView: sender.view!, pullingView: pullingView, hideLabel: true)
            }
            else {
                //view двигается вниз
                viewGoesDown(pushingView: sender.view!, pullingView: pullingView, hideLabel: true)
                
            }
            
        }
        else {
            activeView = 0
        }
        
        print("activeView " + String(activeView))
        
        
    }
    
    @objc func swipeLeft (_ sender: UISwipeGestureRecognizer) {
        print(sender)
        if let tmpView = sender.view as UIView? {
            print(tmpView.tag)
            activeView = tmpView.tag
        }
        
        self.performSegue(withIdentifier: "storiesToStory", sender: self)
    }
    
    func swipeRight (_ sender: UISwipeGestureRecognizer){
        self.performSegue(withIdentifier: "toTypes", sender: self)
    }
    
    func findSubLabel(view: UIView, range: Int) -> UILabel {
        //найти UILabel принаждлежащий UIView
        //если не найдено, то будет возвращен пустой UILabel, создаем его
        let returnView : UILabel = UILabel.init()
        
        var count = 0
        
        //получаем все view вложенные в UIView
        let subviews: NSArray = view.subviews as NSArray
        //если нет таких возвращаем пустой UILabel
        if (subviews.count == 0) {return returnView}
        
        //ищем UILabel
        for subview in subviews {
            if let articleView = subview as? UILabel {
                count += 1
                if (count == range) {
                    return articleView
                }
            }
        }
        return returnView
    }
    
    func findViewByTag(tag: Int) -> UIView {
        //найти UIView по значению поля tag
        //если не найдено, то будет возвращен пустой UIView, создаем его
        var returnView : UIView = UIView.init()
        
        //получаем все view вложенные в stackView
        let subviews: NSArray = storiesView.subviews as NSArray
        //если нет таких возвращаем пустой UIImageView
        if (subviews.count == 0) {return returnView}
        
        //ищем UIView с нужным значением поля tag
        for subview in subviews {
            if let articleView = subview as? UIView {
                if articleView.tag == tag {
                    returnView = articleView
                    break
                }
            }
        }
        return returnView
    }
    
    func findViewSize(tag: Int) -> CGSize {
        //найти размер UIView со значением поля tag
        //если не найдено, то будет возвращен 0,0
        var returnSize : CGSize = CGSize(width: 0, height: 0)
        
        //получаем все view вложенные в stackView
        let subviews: NSArray = storiesView.subviews as NSArray
        //если нет таких возвращаем пустой UIImageView
        if (subviews.count == 0) {return returnSize}
        
        //ищем UIView с нужным значением поля tag
        for subview in subviews {
            if let articleView = subview as? UIView {
                if articleView.tag == tag {
                    returnSize = articleView.bounds.size
                    break
                }
            }
        }
        print("FFFFFFFFFFF!!!!!!!!!!!!!!!!!!!!!" + String(describing: returnSize))
        return returnSize
    }
    
    func resetViews () -> Void {
        
        //1. ищем активную UIImageView и его пару
        let pushingView = findViewByTag(tag: activeView)
        let pullingView = findPairView(viewId: activeView)
        
        let pushLabel = findSubLabel(view: pushingView, range: 1)
        let pullingLabel2 = findSubLabel(view: pullingView, range: 1)
        
        UIView.animate(withDuration: 2) {
            pushLabel.isHidden = false
            pullingLabel2.isHidden = false
        }
        
        if (collapseDirection[pushingView.tag-1] != -1) {
            //view двигается вверх
            viewGoesUp(pushingView: pullingView, pullingView: pushingView, hideLabel: false)
        }
        else {
            //view двигается вниз
            viewGoesDown(pushingView: pullingView, pullingView: pushingView, hideLabel: false)
            
        }
        
    }
    
    func viewGoesDown (pushingView: UIView, pullingView: UIView, hideLabel: Bool) {
        //view двигается вниз
        print("viewGoesDown")
        print(pushingView)
        
        let pushLabel = findSubLabel(view: pushingView, range: 1)
        
        UIView.animate(withDuration: 0.2) {
            //двигаем вниз верхную границу, размер уменьшаем
            pullingView.frame = CGRect(
                x: pullingView.frame.origin.x,
                y: pullingView.frame.origin.y + self.articleBlockHeight,
                width: pullingView.frame.width,
                height: pullingView.frame.height - self.articleBlockHeight)
            //self.stackView.bringSubview(toFront: pushingView)
            //увеличиваем размер
            pushingView.frame = CGRect(
                x: pushingView.frame.origin.x,
                y: pushingView.frame.origin.y,
                width: pushingView.frame.width,
                height: pushingView.frame.height + self.articleBlockHeight)
            
            if (hideLabel) {
                pushLabel.isHidden = true
            }
            
            //двигаем label
            // pushLabel.frame = CGRect(x: pushLabel.frame.origin.x, y: 190.0, width: pushLabel.frame.width, height: pushLabel.frame.height)
        }
        
    }
    
    func viewGoesUp (pushingView: UIView, pullingView: UIView, hideLabel: Bool) {
        //view двигается вверх
        print("viewGoesUp")
        let pushLabel = findSubLabel(view: pushingView, range: 1)
        //   let pushLabel2 = findSubLabel(view: pushingView, range: 2)
        UIView.animate(withDuration: 0.2) {
            //двигаем вверх верхную границу, размер увеличиваем
            pushingView.frame = CGRect(
                x: pushingView.frame.origin.x,
                y: pushingView.frame.origin.y - self.articleBlockHeight,
                width: pushingView.frame.width,
                height: pushingView.frame.height + self.articleBlockHeight)
            //сжимаемое view - уменьшаем размер
            pullingView.frame = CGRect(
                x: pullingView.frame.origin.x,
                y: pullingView.frame.origin.y,
                width: pullingView.frame.width,
                height: pullingView.frame.height - self.articleBlockHeight)
            
            if (hideLabel) {pushLabel.isHidden = true}
            //     pushLabel2.isHidden = true
        }
    }
    
    //ищем парный view
    func findPairView(viewId : Int) -> UIView {
        
        var pairId = 0
        
        if (collapseDirection[viewId-1] == -1) {
            pairId = viewId - 1
        }
        else {
            pairId = viewId + 1
        }
        print(pairId)
        return findViewByTag(tag: pairId)
    }

    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
